package com.zempty.kafka_admin.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zempty
 * @ClassName AdminController.java
 * @Description TODO
 * @createTime 2020年10月17日 10:23:00
 */
@RestController
public class AdminController {

    @GetMapping("/test")
    public String test() {
        return "test";
    }
}
