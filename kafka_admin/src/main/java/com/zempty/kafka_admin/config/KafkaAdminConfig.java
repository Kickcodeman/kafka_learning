package com.zempty.kafka_admin.config;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/**
 * @author zempty
 * @ClassName KafkaAdminConfig.java
 * @Description
 * @createTime 2020年10月17日 13:56:00
 */
@Configuration
public class KafkaAdminConfig {

    @Bean
    public AdminClient adminClient() {
        Properties properties = new Properties();
        properties.setProperty(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        AdminClient adminClient = AdminClient.create(properties);
        return adminClient;
    }

}
