package com.zempty.kafka_admin;

import org.apache.kafka.clients.admin.*;
import org.apache.kafka.common.KafkaFuture;
import org.apache.kafka.common.config.ConfigResource;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;
import java.util.concurrent.ExecutionException;

@SpringBootTest
class KafkaAdminApplicationTests {

	@Autowired
	private AdminClient adminClient;

	private static final String TOPIC_NAME = "zempty_topic";

	@Test
	void contextLoads() {
	}

	@Test
	public void testAdminClient() {
		System.out.println(adminClient);
	}

	@Test
	//创建 topic
	public void createTopic() {
		short rs = 1;
		NewTopic newTopic = new NewTopic(TOPIC_NAME, 1, rs);
		CreateTopicsResult topicResult = adminClient.createTopics(Arrays.asList(newTopic));
		System.out.println("========================================");
		System.out.println(topicResult);
	}


	//获取 topic 列表
	@Test
	public void topicList() throws ExecutionException, InterruptedException {
		ListTopicsResult listTopicsResult = adminClient.listTopics();
		Set<String> names =listTopicsResult.names().get();
		System.out.println("===============================================");
		names.forEach(System.out::println);
		System.out.println("===============================================");
	}

	//带参数的listTopics
	@Test
	public void testList2() throws ExecutionException, InterruptedException {
		ListTopicsOptions listTopicsOptions = new ListTopicsOptions();
		listTopicsOptions.listInternal(true);
		ListTopicsResult listTopicsResult = adminClient.listTopics(listTopicsOptions);
		Set<String> names =listTopicsResult.names().get();
		System.out.println("===============================================");
		names.forEach(System.out::println);
		System.out.println("===============================================");
	}

	//打印 TopicListing
	@Test
	public void testList3() throws ExecutionException, InterruptedException {
		ListTopicsOptions listTopicsOptions = new ListTopicsOptions();
		listTopicsOptions.listInternal(true);
		ListTopicsResult listTopicsResult = adminClient.listTopics(listTopicsOptions);
		Collection<TopicListing> collection = listTopicsResult.listings().get();
		collection.forEach(System.out::println);
	}

//	删除 topic
	@Test
	public void deleteTopic() {
		DeleteTopicsResult deleteTopicsResult = adminClient.deleteTopics(Arrays.asList(TOPIC_NAME));
		Map<String,KafkaFuture<Void>> map = deleteTopicsResult.values();
		Set<String> set = map.keySet();
		set.forEach(System.out::println);
	}


	@Test
	//获取 topic  的描述
	public void describeTopic() {
		DescribeTopicsResult describeTopicsResult = adminClient.describeTopics(Arrays.asList(TOPIC_NAME));
		Map<String, KafkaFuture<TopicDescription>> map = describeTopicsResult.values();
		Set<Map.Entry<String, KafkaFuture<TopicDescription>>> set = map.entrySet();
		set.forEach(o->{
			System.out.println(o.getKey());
			System.out.println("=================================================================");
			try {
				System.out.println(o.getValue().get());
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		});
	}


	@Test
	//获取 topic 的配置描述
	public void configDescribe() throws ExecutionException, InterruptedException {
		ConfigResource resource = new ConfigResource(ConfigResource.Type.TOPIC, TOPIC_NAME);
		DescribeConfigsResult describeConfigsResult = adminClient.describeConfigs(Arrays.asList(resource));
		Map<ConfigResource, Config> configResourceConfigMap = describeConfigsResult.all().get();
		Set<Map.Entry<ConfigResource, Config>> entries = configResourceConfigMap.entrySet();
		entries.forEach(o->{
			System.out.println(o.getKey());
			System.out.println("=======================================================================");
			System.out.println(o.getValue());
		});
	}


	@Test
//	修改配置信息
	public void modifyConfig() {
		Map<ConfigResource, Collection<AlterConfigOp>> map = new HashMap<>();
		ConfigResource configResource = new ConfigResource(ConfigResource.Type.TOPIC, TOPIC_NAME);
		ConfigEntry configEntry = new ConfigEntry("preallocate", "true");
		AlterConfigOp alterConfigOp = new AlterConfigOp(configEntry, AlterConfigOp.OpType.SET);
		map.put(configResource, Arrays.asList(alterConfigOp));
		AlterConfigsResult alterConfigsResult = adminClient.incrementalAlterConfigs(map);
		alterConfigsResult.values().keySet().forEach(System.out::println);
	}


	@Test
//	增加 partitions 数量
	public void incrPartitions() {
		Map<String, NewPartitions> map = new HashMap<>();
		NewPartitions newPartitions = NewPartitions.increaseTo(2);
		map.put(TOPIC_NAME, newPartitions);
		CreatePartitionsResult partitions = adminClient.createPartitions(map);
		partitions.values().entrySet().forEach(o-> System.out.println(o.getKey()));
	}

}
