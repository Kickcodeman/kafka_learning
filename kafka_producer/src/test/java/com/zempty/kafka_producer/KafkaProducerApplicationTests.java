package com.zempty.kafka_producer;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * @author zempty
 * @ClassName KafkaProducerApplicationTests.java
 * @Description Producer Api 的学习和理解
 * @createTime 2020年10月17日 22:34:00
 */
@SpringBootTest
public class KafkaProducerApplicationTests {

    @Autowired
    private Producer producer;

    private static final String TOPIC_NAME = "zempty_topic";

    @Test
    public void sendMessage() {
        for (int i = 0; i < 10; i++) {
            ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC_NAME,
                    "zempty" + i);
            producer.send(record);
        }
        producer.close();
    }

    @Test
    public void SyncSend() throws ExecutionException, InterruptedException {
        for (int i = 0; i < 10; i++) {
            String value = "zempty" + i;
            ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC_NAME,
                    value);
            Future<RecordMetadata> send = producer. send(record);
            //在这里会阻塞
            RecordMetadata recordMetadata = send.get();
            System.out.println("================================================");
            System.out.println("value :"+ value + " partition :"+recordMetadata.partition() + " "
            +" offset :"+recordMetadata.offset());

        }
        producer.close();
    }


    @Test
    //测试 callback 功能
    public void callback() {
        for (int i = 0; i < 10; i++) {
            String value = "zempty" + i;
            ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC_NAME,
                    value);
            Future<RecordMetadata> send = producer. send(record, new Callback() {
                @Override
                public void onCompletion(RecordMetadata metadata, Exception exception) {
                    System.out.println("value :"+ value + " partition :"+metadata.partition() + " "
                            +" offset :"+metadata.offset());
                }
            });
        }
        producer.close();
    }


    @Test
    public void sendPartition() {
        for (int i = 0; i < 10; i++) {
            ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC_NAME,0,null,
                    "zempty" + i);
            producer.send(record);
        }
        producer.close();
    }
}
