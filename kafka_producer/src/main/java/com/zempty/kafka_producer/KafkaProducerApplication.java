package com.zempty.kafka_producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zempty
 * @ClassName KafkaProducerApplication.java
 * @Description
 * @createTime 2020年10月17日 00:45:00
 */

@SpringBootApplication
public class KafkaProducerApplication {
    public static void main(String[] args) {
        SpringApplication.run(KafkaProducerApplication.class, args);
    }
}
