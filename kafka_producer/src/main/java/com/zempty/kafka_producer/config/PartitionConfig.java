package com.zempty.kafka_producer.config;

import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.common.Cluster;

import java.util.Map;

/**
 * @author zempty
 * @ClassName PartitionConfig.java
 * @Description 自定义 partition
 * @createTime 2020年10月17日 23:35:00
 */
public class PartitionConfig implements Partitioner {
    @Override
    public int partition(String topic, Object key, byte[] keyBytes, Object value, byte[] valueBytes, Cluster cluster) {
        //zempty1
        String numstr = value + "";
        int num = Integer.parseInt(numstr.substring(6));
        return num%2;
    }

    @Override
    public void close() {

    }

    @Override
    public void configure(Map<String, ?> configs) {

    }
}
