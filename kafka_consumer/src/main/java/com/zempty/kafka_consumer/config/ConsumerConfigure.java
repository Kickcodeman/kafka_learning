package com.zempty.kafka_consumer.config;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/**
 * @author zempty
 * @ClassName ConsumerConfigure.java
 * @Description 初始化 Consumer 配置
 * @createTime 2020年10月18日 13:23:00
 */
@Configuration
public class ConsumerConfigure {

    @Bean
    public Consumer<String,String> kafkaConsumer() {
        Properties properties = new Properties();
        properties.put("bootstrap.servers", "localhost:9092");
        properties.put("group.id", "test");
        properties.put("enable,auto.commit", "false");
//        properties.put("enable,auto.commit", "true");
        properties.put("auto.commit.interval.ms", 1000);
        properties.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        Consumer<String,String> consumer = new KafkaConsumer<String,String>(properties);
        return consumer;
    }
}
