package com.zempty.kafka_consumer;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Duration;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
class KafkaConsumerApplicationTests {

	@Autowired
	private Consumer consumer;

	private static final String TOPIC_NAME = "zempty_topic";

	@Test
	void contextLoads() {
	}

	@Test
	public void subcribe() {
		consumer.subscribe(Arrays.asList(TOPIC_NAME));
		while (true) {
			ConsumerRecords<String,String> records = consumer.poll(Duration.ofMillis(1000));
			for (ConsumerRecord record : records) {
				System.out.println("====================================================");
				System.out.printf("partion = %d ,offset = %d , key = %s , vaule = %s%n ",record.partition(),record.offset(),record.key(),record.value());
			}
		}
	}


	public void commitedOffset() {
		consumer.subscribe(Arrays.asList(TOPIC_NAME));
		while (true) {
			ConsumerRecords<String,String> records = consumer.poll(Duration.ofMillis(1000));
			for (ConsumerRecord record : records) {
				System.out.println("====================================================");

				//把数据提交到数据库 todo
				//todo
				System.out.printf("partion = %d ,offset = %d , key = %s , vaule = %s%n ",record.partition(),record.offset(),record.key(),record.value());

			}
			//手动提交
			consumer.commitAsync();
		}
	}


	@Test
	public void partition() {
		consumer.subscribe(Arrays.asList(TOPIC_NAME));
		while (true) {
			ConsumerRecords<String,String> records = consumer.poll(Duration.ofMillis(1000));
			for (TopicPartition partition : records.partitions()) {
				List<ConsumerRecord<String, String>> pRecords = records.records(partition);
				for (ConsumerRecord record : pRecords) {
					System.out.printf("partion = %d ,offset = %d , key = %s , vaule = %s%n ",record.partition(),record.offset(),record.key(),record.value());
				}
				long lastOffset = pRecords.get(pRecords.size() - 1).offset();
				Map<TopicPartition, OffsetAndMetadata> offset = new HashMap<>();
				offset.put(partition, new OffsetAndMetadata(lastOffset + 1));
				consumer.commitSync(offset);
				System.out.println("===========partition : "+partition +"=======================");
			}
		}
	}


	@Test
	public void partition2() {
		TopicPartition topicPartition = new TopicPartition(TOPIC_NAME, 0);
		TopicPartition topicPartition1 = new TopicPartition(TOPIC_NAME, 1);
		consumer.assign(Arrays.asList(topicPartition));
		while (true) {
			ConsumerRecords<String,String> records = consumer.poll(Duration.ofMillis(1000));
			for (TopicPartition partition : records.partitions()) {
				List<ConsumerRecord<String, String>> pRecords = records.records(partition);
				for (ConsumerRecord record : pRecords) {
					System.out.printf("partion = %d ,offset = %d , key = %s , vaule = %s%n ",record.partition(),record.offset(),record.key(),record.value());
				}
				long lastOffset = pRecords.get(pRecords.size() - 1).offset();
				Map<TopicPartition, OffsetAndMetadata> offset = new HashMap<>();
				offset.put(partition, new OffsetAndMetadata(lastOffset + 1));
				consumer.commitSync(offset);
				System.out.println("===========partition : "+partition +"=======================");
			}
		}
	}






}
